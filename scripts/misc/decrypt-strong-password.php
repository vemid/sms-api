<?php


use Defuse\Crypto\Crypto;
use Doctrine\ORM\EntityManagerInterface;
use Vemid\ProjectOne\Common\Config\ConfigInterface;
use Vemid\ProjectOne\Common\Helper\Cryptor;
use Vemid\ProjectOne\Entity\Entity\Provider;

require_once __DIR__ . '/../init.php';

$entityManager = $container->get(EntityManagerInterface::class);
$cryptor = $container->get(Cryptor::class);
$config = $container->get(ConfigInterface::class);

/** @var Provider $provider */
$provider = $entityManager->find(Provider::class, 1);

$key = unserialize($cryptor->standardDecrypt($config->get('secrets.key')));
$plaintext = Crypto::decrypt($provider->getPassword(), $key);
var_dump($plaintext);