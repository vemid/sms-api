<?php

use Defuse\Crypto\Crypto;
use Doctrine\ORM\EntityManagerInterface;
use Vemid\ProjectOne\Common\Config\ConfigInterface;
use Vemid\ProjectOne\Common\Helper\Cryptor;
use Vemid\ProjectOne\Entity\Entity\Provider;

require_once __DIR__ . '/../init.php';

$password = '';

$cryptor = $container->get(Cryptor::class);
$config = $container->get(ConfigInterface::class);
$entityManager = $container->get(EntityManagerInterface::class);

/** @var Provider $provider */
$provider = $entityManager->find(Provider::class, 1);

$unserialize = unserialize($cryptor->standardDecrypt($config->get('secrets.key')));
$ciphertext = Crypto::encrypt($password, $unserialize);

$provider->setPassword($ciphertext);
//$entityManager->persist($provider);
//$entityManager->flush();