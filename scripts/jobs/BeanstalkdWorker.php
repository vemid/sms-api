<?php

use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManagerInterface;
use Vemid\ProjectOne\Common\Beanstalk\QueueInterface;
use \Vemid\ProjectOne\Common\Beanstalk\Worker;
use Vemid\ProjectOne\Common\Config\ConfigInterface;

require_once __DIR__ . '/../init.php';

$beanstalkQueue = $container->get(QueueInterface::class);
$db = $container->get(Connection::class);
$entityManager = $container->get(EntityManagerInterface::class);
$config = $container->get(ConfigInterface::class);

$worker = new Worker($beanstalkQueue, $db, $entityManager, $config);
$worker->run();

