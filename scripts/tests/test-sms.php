<?php

use Vemid\Sms\Client;

require_once __DIR__ . '/../init.php';

$test = [];
for ($i = 100; $i <= 120; $i++) {
    $test[] = "+3816920{$i}21";
}

$start = microtime(true);
$client = new Client('vemid', 'root');
$messages = $client->messages->sendBatch('ONESTA',$test, 4);
$timeElapsed = microtime(true) - $start;
print_r($timeElapsed);