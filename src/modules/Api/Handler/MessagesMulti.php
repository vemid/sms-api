<?php

declare(strict_types=1);

namespace Vemid\ProjectOne\Api\Handler;

use Doctrine\ORM\EntityManagerInterface;
use Psr\Http\Message\ResponseInterface;
use Vemid\ProjectOne\Common\Beanstalk\QueueInterface;
use Vemid\ProjectOne\Common\Config\ConfigInterface;
use Vemid\ProjectOne\Common\Task\ReceiveMultipleMessageTask;
use Vemid\ProjectOne\Common\Validation\BulkMessagesValidator;
use Vemid\ProjectOne\Common\Route\AbstractHandler;
use Vemid\ProjectOne\Entity\Entity\Sender;

/**
 * Class MessagesMulti
 * @package Vemid\ProjectOne\Api\Handler
 */
class MessagesMulti extends AbstractHandler
{
    /**
     * @param EntityManagerInterface $entityManager
     * @param ConfigInterface $config
     * @param QueueInterface $queue
     * @return array|ResponseInterface|\Zend\Diactoros\Response
     * @throws \Exception
     */
    public function index(EntityManagerInterface $entityManager, ConfigInterface $config, QueueInterface $queue)
    {
        $body = $this->request->getParsedBody();
        $validator = new BulkMessagesValidator();

        if (!$validator->isValid($body)) {
            $response = $this->response->withStatus(400, 'Bad request');
            $response->getBody()->write(json_encode(['error' => $validator->getMessages()]));

            return $response;
        }

        /** @var $sender Sender */
        $sender = $entityManager->getRepository(Sender::class)->findOneByCode($body['sender']);

        if (!$sender || !$sender->getIsActive()) {
            $response = $this->response->withStatus(400, 'Bad request');
            $response->getBody()->write(json_encode(['error' => 'Sender do not exist']));

            return $response;
        }

        if (!$sender->getClient()->getIsActive()) {
            $response = $this->response->withStatus(400, 'Bad request');
            $response->getBody()->write(json_encode(['error' => 'Client do not exist']));

            return $response;
        }

        foreach ($body['recipients'] as $recipientNumber) {
            $task = new ReceiveMultipleMessageTask($entityManager, $config, $queue);
            $task->recipientNumber = $recipientNumber;
            $task->sender = $sender;
            $task->smsText = $body['text'];
            $task->scheduledOn = isset($body['scheduledOn']) ? new \DateTime($body['scheduledOn']) : null;
            $task->runInBackground();
        }

        return ['status' => 'message set into the queue'];
    }
}
