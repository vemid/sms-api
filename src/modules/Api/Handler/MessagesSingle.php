<?php


namespace Vemid\ProjectOne\Api\Handler;

use Doctrine\ORM\EntityManagerInterface;
use Vemid\ProjectOne\Common\Route\AbstractHandler;
use Vemid\ProjectOne\Common\Validation\SingleMessageValidator;
use Vemid\ProjectOne\Entity\Entity\Recipient;
use Vemid\ProjectOne\Entity\Entity\Sender;
use Vemid\ProjectOne\Entity\Entity\Sms;

/**
 * Class MessagesSingle
 * @package Vemid\ProjectOne\Api\Handler
 */

class MessagesSingle extends AbstractHandler
{
    public function index(EntityManagerInterface $entityManager)
    {
        $body = $this->request->getParsedBody();
        $validator = new SingleMessageValidator();

        if (!$validator->isValid($body)) {
            $response = $this->response->withStatus(400, 'Bad request');
            $response->getBody()->write(json_encode(['error' => $validator->getMessages()]));

            return $response;
        }

        /** @var $sender Sender */
        if (!$sender = $entityManager->getRepository(Sender::class)->findOneByCode($body['sender'])) {
            $response = $this->response->withStatus(400, 'Bad request');
            $response->getBody()->write(json_encode(['error' => 'Client do not exist']));

            return $response;
        }

        if (!$recipient = $entityManager->getRepository(Recipient::class)->findOneByPhoneNumber($body['recipient'])) {
            $recipient = new Recipient();
            $recipient->setClient($sender->getClient());
            $recipient->setPhoneNumber($body['recipient']);
            $recipient->setIsActive(1);
            $recipient->setCreated(new \DateTime());

            $entityManager->persist($recipient);
            $entityManager->flush();
        }

        $sms = new Sms();
        $sms->setSender($sender);
        $sms->setRecipient($recipient);
        $sms->setMessageType('SMS');
        $sms->setSmsText($body['text']);
        $sms->setCreated(new \DateTime());

        if (isset($body['scheduledOn'])) {
            $sms->setScheduledOn(new \DateTime($body['scheduledOn']));
        }

        $entityManager->persist($sms);
        $entityManager->flush();

        return ['status' => 'message set into the queue'];



    }

}