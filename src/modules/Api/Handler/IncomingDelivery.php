<?php

declare(strict_types=1);

namespace Vemid\ProjectOne\Api\Handler;

use Doctrine\ORM\EntityManagerInterface;
use Vemid\ProjectOne\Common\Route\AbstractHandler;
use Vemid\ProjectOne\Entity\Entity\Sms;

/**
 * Class IncomingDelivery
 * @package Vemid\ProjectOne\Api\Handler
 */
class IncomingDelivery extends AbstractHandler
{
    public function index(EntityManagerInterface $entityManager)
    {
        $body = $this->request->getParsedBody();
        /** @var Sms $sms */
        $sms = $entityManager->getRepository(Sms::class)->findOneByMessageId($body['messageid']);

        if (!$sms) {
            throw new \LogicException(sprintf('Requested sms record with messageId: %s not found!', $body['messageid']));
        }

        if ($sms->getDelivered() || $sms->getErrorMessage()) {
            return ['status' => 'message delivery info received'];
        }

        if ((int)$body['status'] === Sms::DELIVERED) {
            $sms->setDelivered(true);
        } else {
            $sms->setErrorMessage($body['text']);
        }

        $sms->setDelivered(true);

        $entityManager->persist($sms);
        $entityManager->flush();

        return ['status' => 'message delivery info received'];
    }
}