<?php

declare(strict_types=1);

namespace Vemid\ProjectOne\Entity\Entity;

use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;
use Vemid\ProjectOne\Entity\Entity;

/**
 * Sms
 *
 * @ORM\Table(name="sms", indexes={@ORM\Index(name="campaign_id", columns={"campaign_id"}), @ORM\Index(name="recipient_id", columns={"recipient_id"}), @ORM\Index(name="sender_id", columns={"sender_id"})})
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 */
class Sms extends Entity
{
    public const DELIVERED = 2;

    /**
     * @var \Ramsey\Uuid\UuidInterface
     *
     * @ORM\Id
     * @ORM\Column(type="uuid", unique=true)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class="Ramsey\Uuid\Doctrine\UuidGenerator")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="message_type", type="string", length=0, nullable=false, options={"default"="SMS"})
     */
    private $messageType = 'SMS';

    /**
     * @var string|null
     *
     * @ORM\Column(name="message_id", type="string", length=0, nullable=false)
     */
    private $messageId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="sms_text", type="text", length=0, nullable=true)
     */
    private $smsText;

    /**
     * @var bool
     *
     * @ORM\Column(name="sent_on", type="datetime", nullable=true)
     */
    private $sentOn;

    /**
     * @var bool
     *
     * @ORM\Column(name="delivered", type="boolean", nullable=false, options={"default"="1"})
     */
    private $delivered = true;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="scheduled_on", type="datetime", nullable=true)
     */
    private $scheduledOn;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="delivered_date", type="datetime", nullable=true)
     */
    private $deliveredDate;

    /**
     * @var string|null
     *
     * @ORM\Column(name="error_message", type="blob", length=0, nullable=true)
     */
    private $errorMessage;

    /**
     * @var Sender
     *
     * @ORM\ManyToOne(targetEntity="Sender", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="sender_id", referencedColumnName="id")
     * })
     */
    private $sender;

    /**
     * @var Campaign
     *
     * @ORM\ManyToOne(targetEntity="Campaign", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="campaign_id", referencedColumnName="id")
     * })
     */
    private $campaign;

    /**
     * @var Recipient
     *
     * @ORM\ManyToOne(targetEntity="Recipient")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="recipient_id", referencedColumnName="id")
     * })
     */
    private $recipient;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set messageType.
     *
     * @param string $messageType
     *
     * @return Sms
     */
    public function setMessageType($messageType)
    {
        $this->messageType = $messageType;

        return $this;
    }

    /**
     * Get messageType.
     *
     * @return string
     */
    public function getMessageType()
    {
        return $this->messageType;
    }

    /**
     * @ORM\PrePersist
     */
    public function setMessageId()
    {
        $this->messageId = base_convert(Uuid::uuid4(), 16, 36);
    }

    /**
     * Get messageType.
     *
     * @return string
     */
    public function getMessageId()
    {
        return $this->messageId;
    }

    /**
     * Set smsText.
     *
     * @param string|null $smsText
     *
     * @return Sms
     */
    public function setSmsText($smsText = null)
    {
        $this->smsText = $smsText;

        return $this;
    }

    /**
     * Get smsText.
     *
     * @return string|null
     */
    public function getSmsText()
    {
        return $this->smsText;
    }

    /**
     * Set delivered.
     *
     * @param bool $delivered
     *
     * @return Sms
     */
    public function setDelivered($delivered)
    {
        $this->delivered = $delivered;

        return $this;
    }

    /**
     * Get delivered.
     *
     * @return bool
     */
    public function getDelivered()
    {
        return $this->delivered;
    }

    /**
     * Set scheduledOn.
     *
     * @param \DateTime $scheduledOn
     *
     * @return Sms
     */
    public function setScheduledOn($scheduledOn)
    {
        $this->scheduledOn = $scheduledOn;

        return $this;
    }

    /**
     * Get scheduledOn.
     *
     * @return \DateTime
     */
    public function getScheduledOn()
    {
        return $this->scheduledOn;
    }

    /**
     * Set sentOn.
     *
     * @param \DateTime $sentOn
     *
     * @return Sms
     */
    public function setSentOn($sentOn)
    {
        $this->sentOn = $sentOn;

        return $this;
    }

    /**
     * Get sentOn.
     *
     * @return \DateTime
     */
    public function getSentOn()
    {
        return $this->sentOn;
    }

    /**
     * Set created.
     *
     * @param \DateTime $created
     *
     * @return Sms
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created.
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set deliveredDate.
     *
     * @param \DateTime $deliveredDate
     *
     * @return Sms
     */
    public function setDeliveredDate($deliveredDate)
    {
        $this->deliveredDate = $deliveredDate;

        return $this;
    }

    /**
     * Get deliveredDate.
     *
     * @return \DateTime
     */
    public function getDeliveredDate()
    {
        return $this->deliveredDate;
    }

    /**
     * Set errorMessage.
     *
     * @param string|null $errorMessage
     *
     * @return Sms
     */
    public function setErrorMessage($errorMessage = null)
    {
        $this->errorMessage = $errorMessage;

        return $this;
    }

    /**
     * Get errorMessage.
     *
     * @return string|null
     */
    public function getErrorMessage()
    {
        return $this->errorMessage;
    }

    /**
     * Set sender.
     *
     * @param Sender|null $sender
     *
     * @return Sms
     */
    public function setSender(Sender $sender = null)
    {
        $this->sender = $sender;

        return $this;
    }

    /**
     * Get sender.
     *
     * @return Sender|null
     */
    public function getSender()
    {
        return $this->sender;
    }

    /**
     * Set campaign.
     *
     * @param Campaign|null $campaign
     *
     * @return Sms
     */
    public function setCampaign(Campaign $campaign = null)
    {
        $this->campaign = $campaign;

        return $this;
    }

    /**
     * Get campaign.
     *
     * @return Campaign|null
     */
    public function getCampaign()
    {
        return $this->campaign;
    }

    /**
     * Set recipient.
     *
     * @param Recipient|null $recipient
     *
     * @return Sms
     */
    public function setRecipient(Recipient $recipient = null)
    {
        $this->recipient = $recipient;

        return $this;
    }

    /**
     * Get recipient.
     *
     * @return Recipient|null
     */
    public function getRecipient()
    {
        return $this->recipient;
    }
}
