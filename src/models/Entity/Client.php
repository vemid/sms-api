<?php

declare(strict_types=1);

namespace Vemid\ProjectOne\Entity\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Vemid\ProjectOne\Entity\Entity;

/**
 * Clients
 *
 * @ORM\Table(name="clients")
 * @ORM\Entity
 */
class Client extends Entity
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="company", type="string", length=255, nullable=false)
     */
    private $company;

    /**
     * @var string
     *
     * @ORM\Column(name="username", type="string", length=255, nullable=false)
     */
    private $username;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=255, nullable=false)
     */
    private $password;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=false)
     */
    private $email;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_active", type="boolean", nullable=false)
     */
    private $isActive = '0';

    /**
     * @var string|null
     *
     * @ORM\Column(name="secret_key", type="string", length=255, nullable=true)
     */
    private $secretKey;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="registered_datetime", type="datetime", nullable=true)
     */
    private $registeredDatetime;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="last_visit_datetime", type="datetime", nullable=true)
     */
    private $lastVisitDatetime;

    /**
     * @ORM\OneToMany(targetEntity="Recipient", mappedBy="client", cascade={"persist"})
     */
    private $recipients;

    /**
     * @ORM\OneToMany(targetEntity="Sender", mappedBy="client", cascade={"persist"})
     */
    private $senders;

    public function __construct()
    {
        $this->senders = new ArrayCollection();
        $this->recipients = new ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set company.
     *
     * @param string $company
     *
     * @return Client
     */
    public function setCompany($company)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company.
     *
     * @return string
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set username.
     *
     * @param string $username
     *
     * @return Client
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username.
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set password.
     *
     * @param string $password
     *
     * @return Client
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password.
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set email.
     *
     * @param string $email
     *
     * @return Client
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email.
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set isActive.
     *
     * @param bool $isActive
     *
     * @return Client
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive.
     *
     * @return bool
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Set secretKey.
     *
     * @param string|null $secretKey
     *
     * @return Client
     */
    public function setSecretKey($secretKey = null)
    {
        $this->secretKey = $secretKey;

        return $this;
    }

    /**
     * Get secretKey.
     *
     * @return string|null
     */
    public function getSecretKey()
    {
        return $this->secretKey;
    }

    /**
     * Set registeredDatetime.
     *
     * @param \DateTime|null $registeredDatetime
     *
     * @return Client
     */
    public function setRegisteredDatetime($registeredDatetime = null)
    {
        $this->registeredDatetime = $registeredDatetime;

        return $this;
    }

    /**
     * Get registeredDatetime.
     *
     * @return \DateTime|null
     */
    public function getRegisteredDatetime()
    {
        return $this->registeredDatetime;
    }

    /**
     * Set lastVisitDatetime.
     *
     * @param \DateTime|null $lastVisitDatetime
     *
     * @return Client
     */
    public function setLastVisitDatetime($lastVisitDatetime = null)
    {
        $this->lastVisitDatetime = $lastVisitDatetime;

        return $this;
    }

    /**
     * Get lastVisitDatetime.
     *
     * @return \DateTime|null
     */
    public function getLastVisitDatetime()
    {
        return $this->lastVisitDatetime;
    }
}
