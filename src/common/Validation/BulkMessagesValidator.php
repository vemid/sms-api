<?php

declare(strict_types=1);

namespace Vemid\ProjectOne\Common\Validation;

use Vemid\ProjectOne\Common\Validation\Validators\PhoneNumberValidator;
use Vemid\ProjectOne\Common\Validation\Validators\ScheduleValidator;
use Vemid\Sms\Exceptions\NotValidFieldException;

/**
 * Class BulkMessages
 * @package Vemid\Sms\Validators
 */
class BulkMessagesValidator extends AbstractValidator
{
    /**
     * {@inheritDoc}
     */
    public function validation($payload): bool
    {
        $phoneValidator = new PhoneNumberValidator();
        $scheduleValidator = new ScheduleValidator();

        if (is_string($payload['sender'])) {
            if (empty($payload['sender'])) {
                $this->messages['sender'] = sprintf('Sender is not valid! Sender is empty!');
            }
        } else {
            if (!$phoneValidator->validation($payload['sender'])) {
                $this->messages['sender'] = sprintf('Sender is not in valid format!');
            }
        }

        if (empty($payload['recipients'])) {
            $this->messages['recipients'] = sprintf('Recipient is not valid! No Recipients found!');
        }

        foreach ($payload['recipients'] as $recipient) {
            if (!$phoneValidator->validation($recipient)) {
                $this->messages['recipients'] = sprintf('Number %s is not valid! No Recipients found!', $recipient);
            }
        }

        if (empty($payload['text'])) {
            $this->messages['text'] = sprintf('Text is not valid!Message body is empty!');
        }

        if (isset($payload['scheduledOn'])) {
            if (!$scheduleValidator->validation($payload['scheduledOn'])) {
                $this->messages['sender'] = sprintf('Sender is not in valid format!');
            }
        }

        return true;
    }
}
