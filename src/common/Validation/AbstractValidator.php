<?php

declare(strict_types=1);

namespace Vemid\ProjectOne\Common\Validation;

use Vemid\ProjectOne\Common\Validation\Validators\ValidatorInterface;

/**
 * Class AbstractValidator
 * @package Vemid\ProjectOne\Common\Validation
 */
abstract class AbstractValidator implements ValidatorInterface
{
    /** @var array  */
    protected $messages = [];

    /**
     * @return array
     */
    public function getMessages()
    {
        return $this->messages;
    }

    /**
     * @param array $input
     * @return bool
     */
    public function isValid(array $input): bool
    {
        $this->validation($input);

        return count($this->messages) === 0;
    }
}
