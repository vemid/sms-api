<?php


namespace Vemid\ProjectOne\Common\Validation\Validators;


class ScheduleValidator implements ValidatorInterface
{

    public function validation($input,$format = "Y-m-d H:i:s"): bool
    {

        if(!$input instanceof \DateTime) {
            $dateTime = \DateTime::createFromFormat($format, $input);

            return true;
        }
        return false;

    }

}