<?php

declare(strict_types=1);

namespace Vemid\ProjectOne\Common\Validation\Validators;

/**
 * Interface ValidatorInterface
 * @package Vemid\Sms\Validators
 */
interface ValidatorInterface
{
    /**
     * @param $input
     * @return bool
     */
    public function validation($input): bool;
}