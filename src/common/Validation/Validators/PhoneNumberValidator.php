<?php

declare(strict_types=1);

namespace Vemid\ProjectOne\Common\Validation\Validators;

/**
 * Class PhoneNumberValidator
 * @package Vemid\Sms\Validators
 */
class PhoneNumberValidator implements ValidatorInterface
{
    /**
     * {@inheritDoc}
     */
    public function validation($number): bool
    {
        $phoneUtil = \libphonenumber\PhoneNumberUtil::getInstance();
        $swissNumberProto = $phoneUtil->parse($number, "RS");

        return $phoneUtil->isValidNumberForRegion($swissNumberProto, 'RS');
    }
}