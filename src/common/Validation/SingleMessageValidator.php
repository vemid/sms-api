<?php


namespace Vemid\ProjectOne\Common\Validation;

use Vemid\ProjectOne\Common\Validation\Validators\PhoneNumberValidator;
use Vemid\ProjectOne\Common\Validation\Validators\ScheduleValidator;

/**
 * Class SingleMessageValidator
 * @package Vemid\ProjectOne\Common\Validation
 */
class SingleMessageValidator extends AbstractValidator
{
    public function validation($payload): bool
    {
        $phoneValidator = new PhoneNumberValidator();
        $scheduleValidator = new ScheduleValidator();

        if (is_string($payload['sender'])) {
            if (empty($payload['sender'])) {
                $this->messages['sender'] = sprintf('Sender is not valid! Sender is empty!');
            }
        } else {
            if (!$phoneValidator->validation($payload['sender'])) {
                $this->messages['sender'] = sprintf('Sender is not in valid format!');
            }
        }

        if (empty($payload['recipient'])) {
            $this->messages['recipient'] = sprintf('Recipient is not valid! No Recipients found!');
        }

        if (empty($payload['text'])) {
            $this->messages['text'] = sprintf('Text is not valid!Message body is empty!');
        }

        if (isset($payload['scheduledOn'])) {
            if (!$scheduleValidator->validation($payload['scheduledOn'])) {
                $this->messages['sender'] = sprintf('Sender is not in valid format!');
            }
        }

        return true;


    }

}