<?php

declare(strict_types=1);

namespace Vemid\ProjectOne\Common\Helper;

use Doctrine\ORM\EntityManagerInterface;
use Vemid\ProjectOne\Common\Config\ConfigInterface;
use Vemid\ProjectOne\Entity\Entity\Client;
use \Firebase\JWT\JWT as JwtBuilder;
use Vemid\ProjectOne\Entity\Entity\ClientJwtToken;

/**
 * Class JWT
 * @package Vemid\ProjectOne\Common\Helper
 */
class JWT
{
    /** @var ConfigInterface */
    private $config;

    /** @var Client */
    private $client;

    /** @var EntityManagerInterface */
    private $entityManager;

    /**
     * JWT constructor.
     * @param ConfigInterface $config
     * @param Client $client
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(ConfigInterface $config, Client $client, EntityManagerInterface $entityManager)
    {
        $this->config = $config;
        $this->client = $client;
        $this->entityManager = $entityManager;
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function generateJWT(): string
    {
        $buildData = $this->buildData();
        $token = JwtBuilder::encode($buildData, $this->client->getSecretKey(), 'HS512');

        $clientJwtToken = new ClientJwtToken();
        $clientJwtToken->setClient($this->client);
        $clientJwtToken->setCreated(new \DateTime("@{$buildData['iat']}"));
        $clientJwtToken->setExpiry(new \DateTime("@{$buildData['exp']}"));
        $clientJwtToken->setJwtToken($token);

        $this->entityManager->persist($clientJwtToken);
        $this->entityManager->flush();

        return $token;
    }

    /**
     * @return array
     * @throws \Exception
     */
    private function buildData(): array
    {
        $issuedAt   = time();
        $notBefore  = $issuedAt + 1;
        $expire     = $notBefore + 60;

        return [
            'jti'  => base64_encode(random_bytes(32)),
            'iss'  => $this->config->get('site'),
            'iat'  => $issuedAt,
            'nbf'  => $notBefore,
            'exp'  => $expire,
            'data' => [
                'userId'   => $this->client->getId(),
                'userName' => $this->client->getDisplayName(),
            ]
        ];
    }
}
