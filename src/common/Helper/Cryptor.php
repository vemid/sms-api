<?php

declare(strict_types=1);

namespace Vemid\ProjectOne\Common\Helper;

use Vemid\ProjectOne\Common\Config\ConfigInterface;

/**
 * Class Cryptor
 * @package Vemid\ProjectOne\Common\Helper
 */
class Cryptor
{
    private $config;


    public function __construct(ConfigInterface $config)
    {
        $this->config = $config;
    }

    public function standardCipher($input)
    {
        $cryptKey = $this->config->get('secrets.salt');

        return base64_encode(\mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($cryptKey), $input, MCRYPT_MODE_CBC, md5(md5($cryptKey))));
    }

    public function standardDecrypt($hash)
    {
        $cryptKey = $this->config->get('secrets.salt');

        return rtrim(\mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($cryptKey), base64_decode($hash), MCRYPT_MODE_CBC, md5(md5($cryptKey))), "\0");
    }
}
