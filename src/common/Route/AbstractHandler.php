<?php

declare(strict_types=1);

namespace Vemid\ProjectOne\Common\Route;

use Delight\I18n\I18n;
use Mezzio\Session\SessionInterface;
use Mezzio\Template\TemplateRendererInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Vemid\ProjectOne\Common\Config\ConfigInterface;
use Vemid\ProjectOne\Common\Config\ConfigModuleResolver;
use Vemid\ProjectOne\Common\Factory\StreamLoggerInterface;
use Vemid\ProjectOne\Common\Message\MessageBag;
use Vemid\ProjectOne\Common\Message\MessageInterface;
use Vemid\ProjectOne\Common\Route\Handler\ArgumentResolver\ArgumentResolverManager;
use League\Event\EmitterInterface;
use Monolog\Logger;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Log\LoggerInterface;
use Vemid\ProjectOne\Common\Route\Handler\Context;
use Vemid\ProjectOne\Common\Template\View;
use Vemid\ProjectOne\Common\Translator\TranslationInterface;
use Zend\Diactoros\Response;
use Zend\Expressive\Twig\TwigRenderer;

/**
 * Class AbstractHandler
 * @package Vemid\ProjectOne\Common\Route
 */
abstract class AbstractHandler implements RequestHandlerInterface
{
    /** @var Response */
    protected $response;

    /** @var EmitterInterface */
    protected $eventEmitter;

    /** @var ServerRequestInterface */
    protected $request;

    /** @var ArgumentResolverManager */
    private $argumentResolverManager;

    /** @var StreamLoggerInterface|Logger */
    protected $logger;

    /** @var TwigRenderer */
    protected $template;

    /** @var ConfigInterface */
    protected $config;

    /** @var string */
    private $redirectUrl;

    /** @var string */
    private $forward;

    /** @var SessionInterface */
    protected $session;

    /** @var TranslationInterface */
    protected $translator;

    /** @var View */
    protected $view;

    /**
     * AbstractHandler constructor.
     * @param ResponseInterface $response
     * @param EmitterInterface $eventEmitter
     * @param ArgumentResolverManager $argumentResolverManager
     * @param LoggerInterface $logger
     * @param ConfigInterface $config
     * @param MessageInterface $message
     */
    public function __construct(
        ResponseInterface $response,
        EmitterInterface $eventEmitter,
        ArgumentResolverManager $argumentResolverManager,
        LoggerInterface $logger,
        ConfigInterface $config
    ){
        $this->response = $response;
        $this->eventEmitter = $eventEmitter;
        $this->argumentResolverManager = $argumentResolverManager;
        $this->logger = $logger;
        $this->config = $config;
    }

    /**
     * @param ServerRequestInterface $request
     * @return ResponseInterface
     */
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $attributes = $request->getAttributes();
        $this->request = $request;

        $method = 'index';
        $id = $attributes['id'] ?? null;

        if (array_key_exists('method', $attributes))  {
            $method = ucwords(trim(preg_replace('/[^a-z0-9]+/i', ' ', $attributes['method'])));
            $method = lcfirst(str_replace(' ', '', $method));
        }

        if (!method_exists($this, $method)) {
            header('Content-Type: application/json');

            return $this->response->withStatus(404, 'Page Not Found');
        }

        $arguments = $this->argumentResolverManager->resolve($this, $method);
        if (count($arguments) && !$arguments[0])    {
            $arguments[0] = $id;
        }


        $body = $this->$method(...$arguments);
        if ($body instanceof ResponseInterface) {
            return $body;
        }

        $body = json_encode($body);

        header('Content-Type: application/json');
//        header('Content-Encoding: gzip');
        $this->response->getBody()->write($body);
        $this->response->getBody()->rewind();

        return $this->response;
    }

    /**
     * @param string $context
     * @return string
     */
    private function switchContext(string $context): string
    {
        switch ($context) {
            case Context::HTML:
            default:
                return 'text/html; charset=utf-8';
                break;
            case Context::JSON:
                return 'application/json';
                break;
        }
    }

    /**
     * @param string $redirectUrl
     */
    protected function redirect(string $redirectUrl): void
    {
        $this->redirectUrl = $redirectUrl;
    }

    /**
     * @param string $url
     */
    protected function forward(string $url): void
    {
        $this->forward = $url;
    }
}
