<?php

declare(strict_types=1);

namespace Vemid\ProjectOne\Common\Console\Command;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Vemid\ProjectOne\Common\Beanstalk\QueueInterface;
use Vemid\ProjectOne\Common\Config\ConfigInterface;
use Vemid\ProjectOne\Common\Helper\Cryptor;
use Vemid\ProjectOne\Common\Sms\ProviderFactory;
use Vemid\ProjectOne\Common\Task\SendBulkMessagesTask;
use Vemid\ProjectOne\Entity\Entity\Sender;
use Vemid\ProjectOne\Entity\Entity\Sms;

/**
 * Class SendMessagesCommand
 * @package Vemid\ProjectOne\Common\Console\Command
 */
class SendMessagesCommand extends CommandBase
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('sms:bulk')
            ->setDescription('Sending Bulk messages using sms provider');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void
     * @throws \DI\DependencyException
     * @throws \DI\NotFoundException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $entityManager = $this->container->get(EntityManagerInterface::class);
        $config = $this->container->get(ConfigInterface::class);
        $cryptor = $this->container->get(Cryptor::class);
        $queue = $this->container->get(QueueInterface::class);

        /** @var Sender[] $senders */
        $senders = $entityManager->getRepository(Sender::class)->findBy([
            'isActive' => true
        ]);

        foreach ($senders as $sender) {
            $messages = $entityManager->getRepository(Sms::class)
                ->createQueryBuilder('sms')
                ->where('sms.sentOn IS NULL')
                ->andWhere('sms.sender = :senderId')
                ->andWhere('sms.scheduledOn IS NULL OR sms.scheduledOn <= :scheduledOn')
                ->setParameters([
                    'senderId' => $sender->getId(),
                    'scheduledOn' => (new \DateTime())->format('Y-m-d H:i:s')
                ])
                ->getQuery()
                ->getResult();

            if (count($messages) === 0) {
                continue;
            }

            $chunkedMessages = array_chunk($messages, 10, true);

            foreach ($chunkedMessages as $smsMessages) {
                $providerFactory = new ProviderFactory($sender, $config, $cryptor);
                $smsProvider = $providerFactory->create();

                $task = new SendBulkMessagesTask($entityManager, $config, $queue);
                $task->messages = $smsMessages;
                $task->provider = $smsProvider;
                $task->runInBackground();
            }
        }
    }
}
