<?php

declare(strict_types=1);

namespace Vemid\ProjectOne\Common\Task;

use Vemid\ProjectOne\Common\Beanstalk\TaskAbstract;
use Vemid\ProjectOne\Common\Sms\Providers\ProviderInterface;

/**
 * Class SendBulkMessagesTask
 * @package Vemid\ProjectOne\Common\Task
 */
class SendBulkMessagesTask extends TaskAbstract
{
    /** @var array */
    public $messages;

    /** @var ProviderInterface */
    public $provider;

    /**
     * @inheritDoc
     */
    public function execute()
    {
        $this->provider->sendStandardMessages($this->messages, $this->entityManager);
    }
}