<?php

declare(strict_types=1);

namespace Vemid\ProjectOne\Common\Task;

use Vemid\ProjectOne\Common\Beanstalk\TaskAbstract;
use Vemid\ProjectOne\Entity\Entity\Recipient;
use Vemid\ProjectOne\Entity\Entity\Sender;
use Vemid\ProjectOne\Entity\Entity\Sms;

/**
 * Class ReceiveMultipleMessage
 * @package Vemid\ProjectOne\Common\Task
 */
class ReceiveMultipleMessageTask extends TaskAbstract
{
    /** @var string */
    public $recipientNumber;

    /** @var Sender */
    public $sender;

    /** @var string */
    public $smsText;

    /** @var null|\DateTime */
    public $scheduledOn;

    /**
     * @inheritDoc
     */
    public function execute()
    {
        if (!$recipient = $this->entityManager->getRepository(Recipient::class)->findOneByPhoneNumber($this->recipientNumber)) {
            $recipient = new Recipient();
            $recipient->setClient($this->sender->getClient());
            $recipient->setPhoneNumber($this->recipientNumber);
            $recipient->setIsActive(1);
            $recipient->setCreated(new \DateTime());

            $this->entityManager->persist($recipient);
            $this->entityManager->flush();
        }

        $sms = new Sms();
        $sms->setSender($this->sender);
        $sms->setRecipient($recipient);
        $sms->setMessageType('SMS');
        $sms->setSmsText($this->smsText);
        $sms->setCreated(new \DateTime());
        $sms->setScheduledOn($this->scheduledOn);

        $this->entityManager->persist($sms);
        $this->entityManager->flush();
    }
}
