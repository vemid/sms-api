<?php

declare(strict_types=1);

namespace Vemid\ProjectOne\Common\Sms\Providers;

use Defuse\Crypto\Crypto;
use Doctrine\ORM\EntityManagerInterface;
use Vemid\ProjectOne\Entity\Entity\Sms;
use \libphonenumber\PhoneNumberUtil;

/**
 * Class NthProvider
 * @package Vemid\ProjectOne\Common\Sms\Providers
 */
class NthProvider extends AbstractProvider
{
    /**
     * {@inheritDoc}
     */
    public function sendStandardMessages(array $messages, EntityManagerInterface $entityManager): string
    {
        /** @var $messages Sms[] */
        if (count($messages) > 10) {
            throw new \LogicException(sprintf('Maximum number of messages is 10 for bulk sending, %s given', count($messages > 10)));
        }

        if (!$provider = $this->sender->getProvider()) {
            throw  new \LogicException(sprintf('No active providers for %s', $this->sender->getName()));
        }

        $hash = $this->config->get('secrets.key');
        $key = unserialize($this->cryptor->standardDecrypt($hash));
        $password = Crypto::decrypt($provider->getPassword(), $key);

        $data = [
            'username' => $provider->getUsername(),
            'password' => $password,
            'status_url' =>  sprintf('%s/delivery', $this->config->get('site')),
            'origin' => $this->sender->getName()
        ];

        if (count($messages) === 1) {
            $data = array_merge($data, $this->buildHttpPayload(array_pop($messages), 0));
        } else {
            $data['multiple'] = count($messages);

            $counter = 1;
            foreach ($messages as $sms) {
                $data = array_merge($data, $this->buildHttpPayload($sms, $counter));
                $counter++;
            }
        }

        $url = sprintf(
            'http://%s:%s?%s',
            $provider->getHost(),
            $provider->getPort(),
            http_build_query($data)
        );

        $response = $this->client->sendRequest($url);

        $sentOn = new \DateTime();
        if (preg_match('/Result_code: 00/', $response)) {
            foreach ($messages as $message) {
                $message->setSentOn($sentOn);
                $entityManager->persist($message);
            }

            $entityManager->flush();
        }

        return $response;
    }

    /**
     * {@inheritDoc}
     */
    public function buildHttpPayload(Sms $sms, $index = 0): array
    {
        return [
            sprintf('call-number%s', $index ?: '') => $this->prepareInternationalNumber($sms->getRecipient()->getPhoneNumber()),
            sprintf('text%s', $index ?: '') => (string)$sms->getSmsText(),
            sprintf('messageid%s', $index ?: '') => $sms->getMessageId(),
        ];
    }

    /**
     * @param string $phoneNumber
     * @return string
     * @throws \libphonenumber\NumberParseException
     * @todo implement internationality (ATM hardcoded RS)
     */
    private function prepareInternationalNumber(string $phoneNumber): string
    {
        $phoneUtil = PhoneNumberUtil::getInstance();
        $phoneNumberProto = $phoneUtil->parse($phoneNumber, 'RS');

        return str_replace([' ', '+'], ['', '00'], $phoneUtil->format($phoneNumberProto, \libphonenumber\PhoneNumberFormat::INTERNATIONAL));
    }
}
