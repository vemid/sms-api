<?php

declare(strict_types=1);

namespace Vemid\ProjectOne\Common\Sms\Providers;

use Vemid\ProjectOne\Common\Config\ConfigInterface;
use Vemid\ProjectOne\Common\Helper\Cryptor;
use Vemid\ProjectOne\Common\Sms\Http\ClientInterface;
use Vemid\ProjectOne\Common\Sms\Http\GuzzleClient;
use Vemid\ProjectOne\Entity\Entity\Sender;

/**
 * Class AbstractProvider
 * @package Vemid\ProjectOne\Common\Sms\Providers
 */
abstract class AbstractProvider implements ProviderInterface
{
    /** @var ClientInterface */
    protected $client;

    /** @var Sender */
    protected $sender;

    /** @var ConfigInterface */
    protected $config;

    /** @var Cryptor */
    protected $cryptor;

    /**
     * AbstractProvider constructor.
     * @param Sender $sender
     * @param ConfigInterface $config
     * @param Cryptor $cryptor
     * @param ClientInterface|null $client
     */
    public function __construct(Sender $sender, ConfigInterface $config, Cryptor $cryptor, ClientInterface $client = null)
    {
        $this->client = $client ?: new GuzzleClient();
        $this->sender = $sender;
        $this->config = $config;
        $this->cryptor = $cryptor;
    }
}
