<?php

declare(strict_types=1);

namespace Vemid\ProjectOne\Common\Sms\Providers;

use Doctrine\ORM\EntityManagerInterface;
use Vemid\ProjectOne\Entity\Entity\Sms;

/**
 * Interface ProviderInterface
 * @package Vemid\ProjectOne\Common\Sms\Providers
 */
interface ProviderInterface
{
    /**
     * @param array $messages
     * @param $entityManager EntityManagerInterface
     * @return string
     * @throws \Defuse\Crypto\Exception\EnvironmentIsBrokenException
     * @throws \Defuse\Crypto\Exception\WrongKeyOrModifiedCiphertextException
     * @throws \Vemid\Sms\Exceptions\HttpException
     */
    public function sendStandardMessages(array $messages, EntityManagerInterface $entityManager): string;

    /**
     * @param Sms $sms
     * @param int $index
     * @return array
     */
    public function buildHttpPayload(Sms $sms, $index = 0): array;
}
