<?php

declare(strict_types=1);

namespace Vemid\ProjectOne\Common\Sms;

use Vemid\ProjectOne\Common\Config\ConfigInterface;
use Vemid\ProjectOne\Common\Helper\Cryptor;
use Vemid\ProjectOne\Common\Sms\Http\ClientInterface;
use Vemid\ProjectOne\Common\Sms\Providers\NthProvider;
use Vemid\ProjectOne\Common\Sms\Providers\ProviderInterface;
use Vemid\ProjectOne\Entity\Entity\Sender;

/**
 * Class ProviderFactory
 * @package Vemid\ProjectOne\Common\Sms\Providers
 */
class ProviderFactory
{
    /** @var ClientInterface */
    private $httpClient;

    /** @var Sender */
    private $sender;

    /** @var ConfigInterface */
    private $config;

    /** @var Cryptor */
    private $cryptor;

    /**
     * ProviderFactory constructor.
     * @param Sender $sender
     * @param ConfigInterface $config
     * @param Cryptor $cryptor
     * @param ClientInterface|null $httpClient
     */
    public function __construct(Sender $sender, ConfigInterface $config, Cryptor $cryptor, ClientInterface $httpClient = null)
    {
        $this->httpClient = $httpClient;
        $this->sender = $sender;
        $this->config = $config;
        $this->cryptor = $cryptor;
    }

    /**
     * @return ProviderInterface
     */
    public function create(): ProviderInterface
    {
        $provider = $this->sender->getProvider();
        switch ($provider->getCode()) {
            case 'NTH':
                return new NthProvider($this->sender, $this->config, $this->cryptor, $this->httpClient);
            default :
                throw new \LogicException(sprintf('Provider not found with code: %s', $provider->getCode()));
        }
    }
}
