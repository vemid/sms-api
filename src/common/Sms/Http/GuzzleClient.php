<?php

declare(strict_types=1);

namespace Vemid\ProjectOne\Common\Sms\Http;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use Vemid\Sms\Exceptions\HttpException;
use Zend\Diactoros\Request;

/**
 * Class GuzzleClient
 * @package Vemid\ProjectOne\Common\Sms\Http
 */
class GuzzleClient implements ClientInterface
{
    /** @var Client */
    private $client;

    /** @var array */
    private $options = [];

    /**
     * GuzzleClient constructor.
     */
    public function __construct()
    {
        $this->client = new Client();
    }

    /**
     * {@inheritDoc}
     */
    public function sendRequest(string $url, $method = 'GET', array $data = []): string
    {
        if (count($data)) {
            $data = ['form_params' => $data];
        }

        $data = array_merge($this->options, $data);

        try {
            $request = new Request($url, $method);
            $response = $this->client->send($request, $data);

        } catch (BadResponseException $exception) {
            $response = $exception->getResponse();
        } catch (\Exception $exception) {
            throw new HttpException('Unable to complete the HTTP request', 500);
        }

        return $response->getBody()->getContents();
    }

    /**
     * @inheritDoc
     */
    public function setBasicAuth(string $username, string $password): void
    {
        $auth = [
            'auth' => [$username, $password]
        ];

        $this->options = array_merge($this->options, $auth);
    }
}
