<?php

declare(strict_types=1);

namespace Vemid\ProjectOne\Common\Sms\Http;

use Vemid\Sms\Exceptions\HttpException;

/**
 * Interface ClientInterface
 * @package Vemid\ProjectOne\Common\Sms\Http
 */
interface ClientInterface
{
    /**
     * @param string $url
     * @param string $method
     * @param array $data
     * @return string
     * @throws HttpException
     */
    public function sendRequest(string $url, $method = 'GET', array $data = []): string;

    /**
     * @param string $username
     * @param string $password
     */
    public function setBasicAuth(string $username, string $password): void;
}
