<?php

declare(strict_types=1);

namespace Vemid\ProjectOne\Common\Beanstalk;

use Pheanstalk\Pheanstalk;

/**
 * Class Queue
 * @package Arbor\Crm\Task
 */
class Queue extends Pheanstalk implements QueueInterface
{
    /** @var string */
    private $tube = 'sms';

    /**
     * {@inheritDoc}
     */
    public function enqueue(TaskAbstract $task, $priority = 1024, $delay = 0, $ttr = 1800): ?int
    {
        $jobId = null;
        $maxAttempts = 3;

        for ($attempt = 1; $attempt <= $maxAttempts; $attempt++) {
            try {
                $data = json_encode($task->toArray());
                $this->useTube($this->tube);
                $job = $this->put($data, $priority, $delay, $ttr);
                $jobId = $job->getId();
                break;
            } catch (\Exception $e) {
                if ($attempt < $maxAttempts) {
                    usleep(200000);
                    continue;
                }
            }
        }

        return $jobId;
    }

    /**
     * {@inheritDoc}
     */
    public function getTube(): string
    {
        return $this->tube;
    }

    /**
     * {@inheritDoc}
     */
    public function setTube($tube): QueueInterface
    {
        $this->tube = $tube;

        return $this;
    }
}
