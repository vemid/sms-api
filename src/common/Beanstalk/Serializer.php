<?php

declare(strict_types=1);

namespace Vemid\ProjectOne\Common\Beanstalk;

use Doctrine\ORM\EntityManagerInterface;
use Vemid\ProjectOne\Entity\EntityInterface;

/**
 * Class Serializer
 * @package Vemid\ProjectOne\Common\Beanstalk
 */
class Serializer
{
    /**
     * @param $phpArray
     * @return array
     */
    public function serializeToSimpleArray($phpArray): array
    {
        if (is_array($phpArray)) {
            foreach ($phpArray as $paramName => $param) {
                try {
                    $phpArray[$paramName] = $this->serializeVariable($param);
                } catch (\InvalidArgumentException $e) {
                    throw new \InvalidArgumentException(sprintf(
                        'Do not know how to serialize property $%s = %s.',
                        $paramName, get_class($param)
                    ));
                }
            }
        } elseif (is_object($phpArray)) {
            throw new \InvalidArgumentException(sprintf(
                'You must provide an array of key/value pairs to encode objects. %s passed.',
                get_class($phpArray)
            ));
        } else {
            $phpArray = $this->serializeVariable($phpArray);
        }

        return $phpArray;
    }

    /**
     * @param array $array
     * @return string|null
     * @throws \InvalidArgumentException
     */
    public function serializeToJson($array): ?string
    {
        if ($array === null) {
            return null;
        }

        return json_encode($this->serializeToSimpleArray($array));
    }

    /**
     * @param $json
     * @param EntityManagerInterface $entityManager
     * @return array
     * @throws \Exception
     */
    public function unserializeFromJson($json, EntityManagerInterface $entityManager): array
    {
        if ($json === null) {
            return [];
        }

        if (is_array($json)) {
            foreach ($json as $paramName => $param) {
                $json[$paramName] = $this->unSerializeVariable($param, $entityManager);
            }
        } else {
            $json = $this->unSerializeVariable($json, $entityManager);
        }

        return $json;
    }


    /**
     * @param $variable
     * @return array|string
     */
    public function serializeVariable($variable)
    {
        if (is_object($variable)) {
            if ($variable instanceof EntityInterface) {
                $variable = $this->entityToArray($variable);
            } elseif ($variable instanceof \DateTime) {
                $variable = $this->dateTimeToArray($variable);
            } else {
                throw new \InvalidArgumentException(sprintf(
                    'Do not know how to serialize property %s.',
                    get_class($variable)
                ));
            }
        }

        return $variable;
    }

    /**
     * @param $variable
     * @param EntityManagerInterface $entityManager
     * @return \ArrayObject|bool|\DateTime|object|null
     * @throws \Exception
     */
    public function unSerializeVariable($variable, EntityManagerInterface $entityManager)
    {
        if (is_array($variable)) {
            if (array_key_exists('entity', $variable)) {
                $variable = $this->entityFromArray($variable, $entityManager);
            } elseif (array_key_exists('collection', $variable)) {
                $collection = new \ArrayObject();
                foreach ($variable['collection'] as $entityArray) {
                    $collection->append($this->entityFromArray($entityArray, $entityManager));
                }
                $variable = $collection;
            } elseif (array_key_exists('date', $variable)) {
                $variable = $this->dateTimeFromArray($variable);
            }
        }

        return $variable;
    }

    /**
     * @param $object
     * @return array
     */
    public function getParamsFromObject($object): array
    {
        $currentParams = $this->serializeToSimpleArray(get_object_vars($object));
        $defaultParams = $this->serializeToSimpleArray(get_class_vars(get_class($object)));

        $params = [];
        foreach ($currentParams as $paramName => $param) {
            if ($param !== $defaultParams[$paramName]) {
                $params[$paramName] = $param;
            }
        }

        return $params;
    }

    /**
     * @param $object
     * @param array $params
     * @throws \InvalidArgumentException
     */
    public function setObjectPropertiesFromParams($object, array $params): void
    {
        foreach ($params as $property => $value) {
            if (!property_exists($object, $property)) {
                throw new \InvalidArgumentException(sprintf(
                    '%s does not have public property %s',
                    get_class($object), $property
                ));
            }

            $object->$property = $value;
        }
    }

    /**
     * @param EntityInterface $entity
     * @return array
     */
    protected function entityToArray(EntityInterface $entity): array
    {
        $array = [
            'entity' => get_class($entity),
            'properties' => [
                $entity->getIdentityField() => $entity->getEntityId()
            ]
        ];

        return $array;
    }

    /**
     * @param array $array
     * @param EntityManagerInterface $entityManager
     * @return bool|object|null
     */
    protected function entityFromArray(array $array, EntityManagerInterface $entityManager)
    {
        if (!isset($array['entity'], $array['properties'])) {
            return false;
        }

        $entity = $array['entity'];
        $properties = $array['properties'];

        $entity = '\\' . ltrim($entity, '\\');

        if (!class_exists($entity)) {
            throw new \InvalidArgumentException(sprintf(
                '%s id not valid Entity',
                $entity
            ));
        }

        $conditions = [];

        foreach ($properties as $propertyName => $propertyValue) {
            $conditions[$propertyName] = $propertyValue;
        }

        return $entityManager->getRepository($entity)->findOneBy($conditions);
    }

    /**
     * @param \DateTime $dateTime
     * @return array
     */
    protected function dateTimeToArray(\DateTime $dateTime): array
    {
        return [
            'date' => $dateTime->format('Y-m-d H:i:s'),
            'timezone' => $dateTime->getTimezone()->getName(),
        ];
    }

    /**
     * @param array $array
     * @return \DateTime
     * @throws \Exception
     */
    protected function dateTimeFromArray(array $array): \DateTime
    {
        return new \DateTime($array['date'], new \DateTimeZone($array['timezone']));
    }
}
