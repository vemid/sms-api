<?php

declare(strict_types=1);

namespace Vemid\ProjectOne\Common\Beanstalk;

use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManagerInterface;
use Pheanstalk\Job;
use Vemid\ProjectOne\Common\Config\ConfigInterface;


/**
 * Class Worker
 * @package Vemid\ProjectOne\Common\Beanstalk
 */
class Worker
{
    /** @var Queue */
    protected $queue;

    /** @var Connection */
    protected $db;

    /** @var EntityManagerInterface */
    protected $entityManager;

    /** @var ConfigInterface */
    protected $config;

    /** @var bool */
    protected $terminateAfterCompletingJob = false;

    /**
     * Worker constructor.
     * @param QueueInterface $queue
     * @param Connection $db
     * @param EntityManagerInterface $entityManager
     * @param ConfigInterface $config
     */
    public function __construct(QueueInterface $queue, Connection $db, EntityManagerInterface $entityManager, ConfigInterface $config)
    {
        $this->queue = $queue;
        $this->db = $db;
        $this->entityManager = $entityManager;
        $this->config = $config;
    }

    /**
     * @throws \Exception
     */
    public function run(): void
    {
        $this->registerSignalHandler();
        $this->queue->watch($this->queue->getTube());

        $timeLimit = time() + 3600; // Kill worker after 1 hour

        do {
            $this->reserveAndExecuteJob();

            if (\memory_get_usage(true) > (200 * 1024 * 1024)) { // 200 Mb
                printf("\n[Shutting down worker with high memory usage of %s", \memory_get_usage(true));
                exit(1);
            }

            pcntl_signal_dispatch(); // If you press CTRL+C (SIGINT) or give SIGTERM this will terminate the worker
        } while (time() < $timeLimit && !$this->terminateAfterCompletingJob);

        exit(2); // Ensures that supervisord restarts this thread because of time limit expiring
    }

    /**
     * @throws \Exception
     */
    private function reserveAndExecuteJob(): void
    {
        /** @var Job $job */
        $job = $this->queue->reserve();

        if ($job) {
            if ($job->getData()) {
                $maxAttempts = 2;
                $this->entityManager->clear();
                for ($attempt = 1; $attempt <= $maxAttempts; $attempt++) {
                    try {
                        $task = TaskFactory::factoryFromJson($job->getData(), $this->entityManager, $this->config, $this->queue);
                        $task->run();
                        $this->queue->delete($job);
                        break;
                    } catch (\PDOException $e) {
                        if ($attempt < $maxAttempts && strpos($e->getMessage(), 'gone away') !== false) {
                            $this->db->connect();
                            sleep(1);
                            continue;
                        }
                    } catch (\Exception $e) {
                        $this->queue->bury($job);
                        throw $e;
                    }
                }
            } else {
                $this->queue->delete($job);
            }

        }
    }

    /**
     * Registers TERMINATE and INTERRUPT signals handler
     */
    public function registerSignalHandler(): void
    {
        if (!function_exists('pcntl_signal')) {
            die('Requires pcntl PHP extension. http://php.net/manual/en/intro.pcntl.php');
        }

        \pcntl_signal(\SIGTERM, [$this, 'signalHandler']);
        \pcntl_signal(\SIGINT, [$this, 'signalHandler']);
    }

    /**
     * @param int $signal
     */
    public function signalHandler($signal): void
    {
        switch ($signal) {
            case \SIGTERM:
            case \SIGINT:
                $this->terminateAfterCompletingJob = true;
                printf("\n[Terminating with %s]", $signal === \SIGTERM ? 'SIGTERM' : 'SIGINT');
                break;
            default:
                printf("\n[Terminating with %s signal that was caught]", $signal);
                exit(1);
        }
    }
}
