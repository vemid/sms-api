<?php

declare(strict_types=1);

namespace Vemid\ProjectOne\Common\Beanstalk;

use Doctrine\ORM\EntityManagerInterface;
use Vemid\ProjectOne\Common\Config\ConfigInterface;

/**
 * Class TaskFactory
 * @package Vemid\ProjectOne\Common\Beanstalk
 */
class TaskFactory
{
    /**
     * @param $json
     * @param EntityManagerInterface $entityManager
     * @return TaskAbstract
     * @throws \Exception
     */
    public static function factoryFromJson($json, EntityManagerInterface $entityManager, ConfigInterface $config, Queue $queue): TaskAbstract
    {
        $payload = json_decode($json, true);

        if (!isset($payload['task'])) {
            throw new \RuntimeException('Trying to run Task for undefined Class');
        }

        $task = self::factoryFromName($payload['task'], $entityManager, $config, $queue);
        $task->setParamsFromArray($payload['params'], $entityManager);

        return $task;
    }

    /**
     * @param $taskName
     * @return TaskAbstract
     */
    private static function factoryFromName($taskName, EntityManagerInterface $entityManager, ConfigInterface $config, Queue $queue): TaskAbstract
    {
        $taskName = '\\' . ltrim($taskName, '\\');

        if (!class_exists($taskName)) {
            throw new \RuntimeException(sprintf('Trying to run Task for undefined Class %s', $taskName));
        }

        return new $taskName($entityManager, $config, $queue);
    }
}
