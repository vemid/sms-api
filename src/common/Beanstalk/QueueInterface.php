<?php


namespace Vemid\ProjectOne\Common\Beanstalk;


interface QueueInterface
{
    /**
     * @param TaskAbstract $task
     * @param int $priority
     * @param int $delay
     * @param int $ttr
     * @return int|null
     */
    public function enqueue(TaskAbstract $task, $priority = 1024, $delay = 0, $ttr = 1800): ?int;

    /**
     * @return string
     */
    public function getTube(): string;

    /**
     * @param $tube
     * @return $this
     */
    public function setTube($tube): self;
}