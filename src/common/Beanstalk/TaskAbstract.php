<?php

declare(strict_types=1);

namespace Vemid\ProjectOne\Common\Beanstalk;

use Doctrine\ORM\EntityManagerInterface;
use Pheanstalk\Connection as PheanstalkConnection;
use Pheanstalk\SocketFactory;
use Vemid\ProjectOne\Common\Config\ConfigInterface;

/**
 * Class TaskAbstract
 * @package Vemid\ProjectOne\Common\Beanstalk
 */
abstract class TaskAbstract
{
    const PRIORITY_VERY_LOW = 4096;
    const PRIORITY_LOW = 2048;
    const PRIORITY_NORMAL = 1024;
    const PRIORITY_HIGH = 512;
    const PRIORITY_VERY_HIGH = 256;
    const PRIORITY_URGENT = 128;

    /** @var int */
    protected static $_priority = self::PRIORITY_NORMAL;

    /** @var int */
    protected static $_ttr = 1800;

    /** @var ConfigInterface */
    private $config;

    /** @var Queue */
    private $queue;

    /** @var EntityManagerInterface */
    protected $entityManager;

    /**
     * TaskAbstract constructor.
     * @param EntityManagerInterface $entityManager
     * @param ConfigInterface $config
     * @param QueueInterface $queue
     */
    public function __construct(EntityManagerInterface $entityManager, ConfigInterface $config, QueueInterface $queue = null)
    {
        $this->config = $config;
        $this->entityManager = $entityManager;
        $this->queue = $queue ?: $this->buildQueue();
    }

    /**
     * @return mixed
     */
    abstract public function execute();

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'task' => get_called_class(),
            'params' => $this->getParamsAsArray()
        ];
    }

    /**
     * @return mixed
     */
    public function run()
    {
        return $this->execute();
    }

    /**
     * @param int $delay
     * @return int|null
     */
    public function runInBackground($delay = 0): ?int
    {
        $config = $this->config->get('beanstalk');

        if ($config->get('host') === '0.0.0.0') {
            $this->execute();

            return null;
        }

        $backgroundJobId = $this->queue->enqueue($this, static::$_priority, $delay, static::$_ttr);

        if ($backgroundJobId === null) {
            $this->execute();
        }

        return $backgroundJobId;
    }

    /**
     * @return array
     */
    public function getParamsAsArray(): array
    {
        return (new Serializer())->getParamsFromObject($this);
    }

    /**
     * @param array $params
     * @param EntityManagerInterface $entityManager
     * @throws \Exception
     */
    public function setParamsFromArray(array $params, EntityManagerInterface $entityManager): void
    {
        $serializer = new Serializer();
        $params = $serializer->unserializeFromJson($params, $entityManager);
        $serializer->setObjectPropertiesFromParams($this, $params);
    }

    /**
     * @return QueueInterface
     */
    private function buildQueue(): QueueInterface
    {
        $config = $this->config->get('beanstalk');

        $queue = new Queue(
            new PheanstalkConnection(
                new SocketFactory(
                    $config->get('host'),
                    (int)$config->get('port'),
                    (int)$config->get('connectionTimeout')
                )
            )
        );

        $queue->setTube($config->get('tube'));

        return $queue;
    }
}
