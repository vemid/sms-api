<?php

declare(strict_types=1);

namespace Vemid\ProjectOne\Common\Middleware;

use Doctrine\ORM\EntityManagerInterface;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Vemid\ProjectOne\Common\Config\ConfigInterface;
use Vemid\ProjectOne\Common\Helper\JWT;
use Vemid\ProjectOne\Entity\Entity\Client;
use Vemid\ProjectOne\Entity\Repository\UserRepository;

/**
 * Class HttpAuthentication
 * @package Vemid\ProjectOne\Common\Middleware
 */
class HttpBasicAuthentication implements MiddlewareInterface
{
    /** @var ResponseFactoryInterface */
    private $responseFactory;

    /**
     * @var string|null
     */
    protected $attribute = 'token';

    /**
     * @var array
     */
    protected $users;

    /** @var ConfigInterface */
    protected $config;

    /** @var EntityManagerInterface */
    protected $entityManager;

    /**
     * @var string
     */
    protected $realm = 'Login';

    /**
     * HttpBasicAuthentication constructor.
     * @param ResponseFactoryInterface $responseFactory
     * @param ConfigInterface $config
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(ResponseFactoryInterface $responseFactory, ConfigInterface $config, EntityManagerInterface $entityManager)
    {
        $this->responseFactory = $responseFactory;
        $this->config = $config;
        $this->entityManager = $entityManager;
    }

    /**
     * @param ServerRequestInterface $request
     * @param RequestHandlerInterface $handler
     * @return ResponseInterface
     * @throws \Exception
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        if ($jwt = $this->hasHeaderAuthToken($request)) {
            if ($this->attribute !== null) {
                $request = $request->withAttribute($this->attribute, $jwt);
            }

            return $handler->handle($request);
        }

        $client = $this->login($request);

        if (!$client) {
            return $this->responseFactory->createResponse(401)
                ->withHeader('WWW-Authenticate', sprintf('Basic realm="%s"', $this->realm));
        }

        $jwt = new JWT($this->config, $client, $this->entityManager);

        return $this->responseFactory->createResponse(307)
            ->withHeader('Auth-Token', $jwt->generateJWT());
    }

    /**
     * @param string $realm
     * @return HttpBasicAuthentication
     */
    public function realm(string $realm): self
    {
        $this->realm = $realm;

        return $this;
    }

    /**
     * @param string $attribute
     * @return HttpBasicAuthentication
     */
    public function attribute(string $attribute): self
    {
        $this->attribute = $attribute;

        return $this;
    }

    /**
     * @param ServerRequestInterface $request
     * @return bool|Client
     */
    private function login(ServerRequestInterface $request)
    {
        $authorization = $this->parseHeader($request->getHeaderLine('Authorization'));

        if (!$authorization) {
            return false;
        }

        /** @var UserRepository $userRepository */
        $userRepository = $this->entityManager->getRepository(Client::class);

        /** @var Client $client */
        $client = $userRepository->findOneBy([
            'username' => $authorization['username'],
            'isActive' => true
        ]);

        if (!$client) {
            return false;
        }

        if (!password_verify($authorization['password'], $client->getPassword())) {
            return false;
        }

        return $client;
    }

    /**
     * @param string $header
     * @return array|null
     */
    private function parseHeader(string $header): ?array
    {
        if (strpos($header, 'Basic') !== 0) {
            return null;
        }

        $header = explode(':', base64_decode(substr($header, 6)), 2);

        return [
            'username' => $header[0],
            'password' => isset($header[1]) ? $header[1] : null,
        ];
    }

    /**
     * @param ServerRequestInterface $request
     * @return bool
     */
    private function hasHeaderAuthToken(ServerRequestInterface $request)
    {
        if (strpos($request->getHeaderLine('Authorization'), 'Bearer') !== false) {
            list($jwt) = sscanf($request->getHeaderLine('Authorization'), 'Bearer %s');

            return $jwt;
        }

        return false;
    }
}
