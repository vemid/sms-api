<?php

declare(strict_types=1);

namespace Vemid\ProjectOne\Common\Middleware;

use Doctrine\ORM\EntityManagerInterface;
use Firebase\JWT\JWT;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Vemid\ProjectOne\Entity\Entity\ClientJwtToken;

/**
 * Class HttpBearerTokenAuthorization
 * @package Vemid\ProjectOne\Common\Middleware
 */
class HttpBearerTokenAuthorization implements MiddlewareInterface
{
    /**
     * @var string
     */
    protected $realm = 'Token Validation Failed';

    /**
     * @var string|null
     */
    protected $attribute = 'clientId';

    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var ResponseFactoryInterface */
    private $responseFactory;

    /**
     * HttpBearerTokenAuthorization constructor.
     * @param ResponseFactoryInterface $responseFactory
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(ResponseFactoryInterface $responseFactory, EntityManagerInterface $entityManager)
    {
        $this->responseFactory = $responseFactory;
        $this->entityManager = $entityManager;
    }

    /**
     * @inheritDoc
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $jwt = $request->getAttribute('token');

        /** @var ClientJwtToken $clientJwtToken */
        $clientJwtToken = $this->entityManager->getRepository(ClientJwtToken::class)->findOneByJwtToken($jwt);

        if (!$clientJwtToken) {
            return $this->responseFactory->createResponse(401)
                ->withHeader('WWW-Authenticate', sprintf('Basic realm="%s"', $this->realm));
        }

        try {
            $token = JWT::decode($jwt, $clientJwtToken->getClient()->getSecretKey(), ['HS512']);
        } catch (\Exception $exception) {
            return $this->responseFactory->createResponse(401)
                ->withHeader('WWW-Authenticate', sprintf('Basic realm="%s"', $this->realm));
        }

        if ($token->data->userId !== $clientJwtToken->getClient()->getId()) {
            return $this->responseFactory->createResponse(401)
                ->withHeader('WWW-Authenticate', sprintf('Basic realm="%s"', $this->realm));
        }

        $timeOfRequest = time();

        if ($timeOfRequest < $token->nbf) {
            return $this->responseFactory->createResponse(401)
                ->withHeader('WWW-Authenticate', sprintf('Basic realm="%s"', $this->realm));
        }

        if ($timeOfRequest > $token->exp) {
            return $this->responseFactory->createResponse(401)
                ->withHeader('WWW-Authenticate', sprintf('Basic realm="Token Expired"'));
        }

        $clientJwtToken->setUsed(1);
        $this->entityManager->persist($clientJwtToken);
        $this->entityManager->flush();

        if ($this->attribute !== null) {
            $request = $request->withAttribute($this->attribute, $clientJwtToken->getClient()->getId());
        }

        return $handler->handle($request);
    }

    /**
     * @param string $attribute
     * @return self
     */
    public function attribute(string $attribute): self
    {
        $this->attribute = $attribute;

        return $this;
    }
}
