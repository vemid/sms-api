<?php

use Phinx\Migration\AbstractMigration;

class Providers extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('providers', ['signed' => false]);
        $table->addColumn('code', 'string')
            ->addColumn('name', 'string')
            ->addColumn('host', 'string')
            ->addColumn('port', 'integer', ['default' => null, 'null' => true])
            ->addColumn('auth', 'enum', ['default' => 'BASIC_AUTH', 'values' => ['BASIC_AUTH', 'TOKEN']])
            ->addColumn('username', 'string', ['default' => null, 'null' => true])
            ->addColumn('password', 'blob', ['limit' => \Phinx\Db\Adapter\MysqlAdapter::BLOB_LONG, 'null' => true])
            ->addColumn('token', 'string', ['default' => null, 'null' => true])
            ->create();
    }
}
