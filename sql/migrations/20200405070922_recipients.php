<?php

use Phinx\Migration\AbstractMigration;

class Recipients extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('recipients', ['signed' => false]);
        $table->addColumn('client_id', 'integer', ['signed' => false])
            ->addColumn('phone_number', 'string')
            ->addColumn('name', 'string', ['null' => true])
            ->addColumn('is_active', 'boolean', ['default' => 1])
            ->addColumn('created', 'datetime')
            ->addColumn('deactivated_at', 'datetime', ['null' => true])
            ->addForeignKey('client_id', 'clients', 'id')
            ->create();
    }
}
