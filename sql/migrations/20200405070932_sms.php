<?php

use Phinx\Migration\AbstractMigration;

class Sms extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('sms', ['signed' => false]);
        $table->addColumn('sender_id', 'integer', ['signed' => false])
            ->addColumn('campaign_id', 'integer', ['signed' => false, 'null' => true])
            ->addColumn('recipient_id', 'integer', ['signed' => false])
            ->addColumn('message_type', 'enum', ['default' => 'SMS', 'values' => ['SMS', 'VIBER', 'MAIL']])
            ->addColumn('sms_text', 'blob', ['limit' => \Phinx\Db\Adapter\MysqlAdapter::BLOB_LONG, 'null' => true])
            ->addColumn('delivered', 'boolean', ['default' => 1])
            ->addColumn('scheduled_on', 'datetime', ['default' => null, 'null' => true])
            ->addColumn('created', 'datetime')
            ->addColumn('delivered_date', 'datetime', ['default' => null, 'null' => true])
            ->addColumn('error_message', 'blob', ['limit' => \Phinx\Db\Adapter\MysqlAdapter::BLOB_LONG, 'null' => true])
            ->addForeignKey('sender_id', 'senders', 'id')
            ->addForeignKey('campaign_id', 'campaigns', 'id')
            ->addForeignKey('recipient_id', 'recipients', 'id')
            ->create();
    }
}
