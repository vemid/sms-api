<?php

use Phinx\Migration\AbstractMigration;

class Clients extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('clients', ['signed' => false]);
        $table->addColumn('company', 'string')
            ->addColumn('username', 'string')
            ->addColumn('password', 'string')
            ->addColumn('email', 'string')
            ->addColumn('is_active', 'boolean', ['default' => 0])
            ->addColumn('secret_key', 'string', ['null' => true])
            ->addColumn('registered_datetime', 'datetime', ['null' => true])
            ->addColumn('last_visit_datetime', 'datetime', ['null' => true])
            ->create();

        $query = sprintf('INSERT INTO `clients` (`id`, `company`, `email`, `username`, `password`, `is_active`, `secret_key`, `registered_datetime`, `last_visit_datetime`) 
            VALUES (NULL, \'Vemid\', \'darkovesic3@gmail.com\', \'vemid\', \'%s\', 1, \'%s\', NULL, NULL);', password_hash('root', PASSWORD_BCRYPT), sha1('Vemid is awesome'));

        $this->execute($query);
    }
}
