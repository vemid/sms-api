<?php

declare(strict_types=1);

use FastRoute\RouteCollector;
use Vemid\ProjectOne\Api\Handler\IncomingDelivery;
use Vemid\ProjectOne\Api\Handler\MessagesMulti;
use Vemid\ProjectOne\Api\Handler\MessagesSingle;
use Vemid\ProjectOne\Api\Handler\Ping;

return [
    'routes' => static function (RouteCollector $routeCollector) {

        $routeCollector->addRoute('GET', '', Ping::class);
        $routeCollector->addRoute('GET', '/', Ping::class);
        $routeCollector->addRoute('POST', '/delivery', IncomingDelivery::class);

        $routeCollector->addGroup('/v1', static function (RouteCollector $routeCollector) {
            $routeCollector->addRoute('GET', '', Ping::class);
            $routeCollector->addRoute('GET', '/', Ping::class);
            $routeCollector->addRoute('POST', '/messages/multi', MessagesMulti::class);
            $routeCollector->addRoute('POST', '/messages/single', MessagesSingle::class);
        });
    }
];
