<?php

use \Vemid\ProjectOne\Api\ConfigProvider as ApiConfigProvider;

return [
    'modules' => [
        'default' => ApiConfigProvider::class,
    ]
];
