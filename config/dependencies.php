<?php

declare(strict_types=1);

use Doctrine\Common\EventManager;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManagerInterface;
use FastRoute\Dispatcher;
use Laminas\Permissions\Acl\AclInterface;
use Laminas\Stratigility\MiddlewarePipe;
use Laminas\Stratigility\MiddlewarePipeInterface;
use League\Event\Emitter;
use League\Event\EmitterInterface;
use League\Event\ListenerProviderInterface;
use Mezzio\MiddlewareFactory;
use Mezzio\Router\FastRouteRouter;
use Middlewares\RequestHandler;
use Narrowspark\HttpEmitter\SapiEmitter;
use Pheanstalk\Connection as PheanstalkConnection;
use Pheanstalk\SocketFactory;
use Phoundation\ErrorHandling\RunnerInterface;
use PragmaRX\Google2FA\Google2FA;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Log\LoggerInterface;
use Vemid\ProjectOne\Common\Acl\Roles;
use Vemid\ProjectOne\Common\Acl\RolesInterface;
use Vemid\ProjectOne\Common\Beanstalk\Queue;
use Vemid\ProjectOne\Common\Beanstalk\QueueInterface;
use Vemid\ProjectOne\Common\Config\ConfigInterface;
use Vemid\ProjectOne\Common\Config\ConfigModuleResolver;
use Vemid\ProjectOne\Common\Config\ConfigResolvedInterface;
use Vemid\ProjectOne\Common\Event\EventList;
use Vemid\ProjectOne\Common\Event\EventListInterface;
use Vemid\ProjectOne\Common\Event\EventProvider;
use Vemid\ProjectOne\Common\Factory\ApplicationConfigFactory;
use Vemid\ProjectOne\Common\Factory\ConnectionFactory;
use Vemid\ProjectOne\Common\Factory\EntityManagerFactory;
use Vemid\ProjectOne\Common\Factory\ErrorHandlerFactory;
use Vemid\ProjectOne\Common\Factory\ErrorLoggerFactory;
use Vemid\ProjectOne\Common\Factory\ErrorLoggerInterface;
use Vemid\ProjectOne\Common\Factory\EventManagerFactory;
use Vemid\ProjectOne\Common\Factory\RouteResourceFactory;
use Vemid\ProjectOne\Common\Factory\StreamLoggerFactory;
use Vemid\ProjectOne\Common\Form\Builder\EntityAnnotationReader;
use Vemid\ProjectOne\Common\Form\FormBuilderInterface;
use Vemid\ProjectOne\Common\Helper\Cryptor;
use Vemid\ProjectOne\Common\Mailer\MailManager;
use Vemid\ProjectOne\Common\Mailer\MailManagerInterface;
use Vemid\ProjectOne\Common\Message\Manager;
use Vemid\ProjectOne\Common\Message\MessageInterface;
use Vemid\ProjectOne\Common\Middleware\Acl;
use Vemid\ProjectOne\Common\Middleware\AppMiddlewarePipe;
use Vemid\ProjectOne\Common\Middleware\EventMiddleware;
use Vemid\ProjectOne\Common\Middleware\EventMiddlewareInterface;
use Vemid\ProjectOne\Common\Middleware\HttpBasicAuthentication;
use Vemid\ProjectOne\Common\Middleware\HttpBearerTokenAuthorization;
use Vemid\ProjectOne\Common\Middleware\LoggedUserMiddleware;
use Vemid\ProjectOne\Common\Middleware\MiddlewareAclInterface;
use Vemid\ProjectOne\Common\Middleware\MiddlewareLogger;
use Vemid\ProjectOne\Common\Middleware\MiddlewareLoggerInterface;
use Vemid\ProjectOne\Common\Middleware\MiddlewareResponseInterface;
use Vemid\ProjectOne\Common\Middleware\RouteMiddleware;
use Vemid\ProjectOne\Common\Middleware\UserLocaleMiddleware;
use Vemid\ProjectOne\Common\Route\Handler\ArgumentResolver\ArgumentResolverManager;
use Vemid\ProjectOne\Common\Route\Handler\ArgumentResolver\ServiceArgumentResolver;
use Zend\Diactoros\Response;
use Zend\Diactoros\ResponseFactory;
use Zend\Diactoros\ServerRequestFactory;
use Zend\Expressive\Helper\ServerUrlHelper;
use function DI\autowire;
use function DI\create;
use function DI\factory;

return [
    AclInterface::class => create(\Laminas\Permissions\Acl\Acl::class),
    AppMiddlewarePipe::class => autowire(AppMiddlewarePipe::class),
    ArgumentResolverManager::class => static function (ContainerInterface $container) {
        return new ArgumentResolverManager(
            $container->get(ServiceArgumentResolver::class)
        );
    },
    QueueInterface::class => static function (ContainerInterface $container) {
        $config = $container->get(ConfigInterface::class);
        $config = $config->get('beanstalk');

        $queue = new Queue(
            new PheanstalkConnection(
                new SocketFactory(
                    $config->get('host'),
                    (int)$config->get('port'),
                    (int)$config->get('connectionTimeout')
                )
            )
        );

        $queue->setTube($config->get('tube'));

        return $queue;
    },
    FormBuilderInterface::class => autowire(EntityAnnotationReader::class),
    ConfigResolvedInterface::class => static function (ContainerInterface $container) {
        $configModuleResolver = $container->get(ConfigModuleResolver::class);
        $config = $container->get(ConfigInterface::class);

        return array_merge_recursive((array)$config, $configModuleResolver->resolve());
    },
    ConfigInterface::class => factory([ApplicationConfigFactory::class, 'create']),
    ConfigModuleResolver::class => autowire(ConfigModuleResolver::class),
    Connection::class => factory([ConnectionFactory::class, 'create']),
    Cryptor::class => autowire(Cryptor::class),
    Dispatcher::class => factory([RouteResourceFactory::class, 'create']),
    EmitterInterface::class => create(Emitter::class),
    EntityManagerInterface::class => factory([EntityManagerFactory::class, 'create']),
    ErrorLoggerInterface::class => factory([ErrorLoggerFactory::class, 'create']),
    EventMiddlewareInterface::class => autowire(EventMiddleware::class),
    EventListInterface::class => create(EventList::class),
    EventManager::class => factory([EventManagerFactory::class, 'create']),
    Google2FA::class => create(Google2FA::class),
    HttpBasicAuthentication::class => autowire(HttpBasicAuthentication::class),
    ListenerProviderInterface::class => autowire(EventProvider::class),
    LoggedUserMiddleware::class => autowire(LoggedUserMiddleware::class),
    LoggerInterface::class => factory([StreamLoggerFactory::class, 'create']),
    MessageInterface::class => create(Manager::class),
    MailManagerInterface::class => autowire(MailManager::class),
    MiddlewareAclInterface::class => autowire(Acl::class),
    MiddlewareLoggerInterface::class => static function (ContainerInterface $container) {
        return new MiddlewareLogger($container->get(LoggerInterface::class));
    },
    MiddlewareFactory::class => autowire(MiddlewareFactory::class),
    MiddlewarePipeInterface::class => create(MiddlewarePipe::class),
    MiddlewareResponseInterface::class => static function (ContainerInterface $container) {
        $middleware = $container->get(AppMiddlewarePipe::class);
        $middleware->pipe($container->get(RouteMiddleware::class));
        $middleware->pipe($container->get(MiddlewareLoggerInterface::class));
        $middleware->pipe('/v1', $container->get(HttpBasicAuthentication::class)->attribute('token'));
        $middleware->pipe('/v1', $container->get(HttpBearerTokenAuthorization::class)->attribute('clientId'));
        $middleware->pipe($container->get(EventMiddlewareInterface::class));
        $middleware->pipe($container->get(RequestHandler::class));

        return $middleware->handle($container->get(ServerRequestInterface::class));
    },
    ResponseInterface::class => create(Response::class),
    ResponseFactoryInterface::class => create(ResponseFactory::class),
    RequestHandler::class => static function (ContainerInterface $container) {
        return new RequestHandler($container);
    },
    RolesInterface::class => autowire(Roles::class),
    RouteMiddleware::class => autowire(RouteMiddleware::class),
    RunnerInterface::class => static function (ContainerInterface $container) {
        return new ErrorHandlerFactory(
            $container->get(ErrorLoggerInterface::class),
            $container->get(ConfigInterface::class)
        );
    },
    SapiEmitter::class => create(SapiEmitter::class),
    ServiceArgumentResolver::class => static function (ContainerInterface $container) {
        return new ServiceArgumentResolver($container);
    },
    ServerRequestInterface::class => static function () {
        return ServerRequestFactory::fromGlobals();
    },
    ServerUrlHelper::class => create(ServerUrlHelper::class),
    UserLocaleMiddleware::class => autowire(UserLocaleMiddleware::class),
    'Zend\Expressive\Router\RouterInterface' => static function (ContainerInterface $container) {
        return new FastRouteRouter(
            null,
            $container->get(ConfigInterface::class)['routes']
        );
    },
];
