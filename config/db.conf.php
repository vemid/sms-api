<?php

declare(strict_types=1);

return [
    'db.host' => getenv('SMS_DB_HOST') ?: 'dbs',
    'db.name' => getenv('SMS_DB_NAME') ?: 'sms',
    'db.port' => getenv('SMS_DB_PORT') ?: '3306',
    'db.username' => getenv('SMS_DB_USER') ?: 'root',
    'db.password' => getenv('SMS_DB_PASSWORD') ?: 'root',
    'db.debug' => getenv('SMS_ENVIRONMENT') ?: true,
];
