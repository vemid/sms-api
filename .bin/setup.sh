#!/bin/sh

HOME_DIR="${PWD}";
HOOK_DIR="${PWD}/.git-hooks";

if [ ! -L .git/hooks ];
then
  git init
  echo ".git/hooks is not symlink"
  echo "copying .git/hooks to .git/old_hooks"
  mv .git/hooks .git/old_hooks

  echo "symlinking $HOOK_DIR/.git-hooks .git/hooks"
  ln -s ${HOOK_DIR} .git/hooks
  git config core.hooksPath .git/hooks
else
  echo ".git/hooks is already a symlink"
fi

if [ ! -d "${HOME_DIR}"/var ];
then
  echo "creating var folder in $HOME_DIR"
  mkdir public/assets
  chmod 777 "${HOME_DIR}/var"
else
  echo "var folder exists"
fi

if [ ! -d "${HOME_DIR}/var/cache/doctrine" ];
then
  echo "creating doctrine folder in $HOME_DIR/var/cache"
  mkdir -p var/cache/doctrine
  chmod 777 var/cache/doctrine
else
  echo "doctrine folder exists"
fi

echo "setup finished"

